#!/usr/bin/env python2

import socket
import sys

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

connection = s.connect_ex(("127.0.0.1", int(sys.argv[1])))

s.close()
sys.exit(connection)

#!/bin/bash

set -euo pipefail

cd gitlab

mkdir -p logs
rm -f logs/*

echo "Extracting the files..."

for f in *.tar.gz; do
  tar -xzf $f
done

(find . -type f -name "production_json.log" | xargs cat > logs/production_json.log.tmp) || true
(find . -type f -name "api_json.log" | xargs cat > logs/api_json.log.tmp) || true
(find . -type f -name "current" | grep gitaly  | xargs cat > logs/gitaly_current.tmp) || true
(find . -type f -name "current" | grep sidekiq  | xargs cat > logs/sidekiq_current.tmp) || true

cd logs

rename.ul '.tmp' '' *

cd ../../

echo "Waiting for Druid to start up..."
while ! ./scripts/check-port.py 8888; do
    sleep 1
done

echo "Uploading the logs..."

for f in specs/*; do
  if ! head -n1 $f | jq; then
    echo "WARNING: $f is not a valid json file"
    continue
  fi
  ./bin/post-index-task --file $f --url http://localhost:8081
done

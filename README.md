# Dashboards for GitLabSOS

Generate interactive dashboards for analyzing one or more GitLabSOS files.

It uses Apache Druid as the time series database and Apache Superset for the dashboards.

## Requirements

* Docker and Docker Compose
* `curl`
* `jq`

## Usage

1. Create a new `./gitlab` directory in the root of the repository.
1. Place the output of GitLabSOS in the `./gitlab` directory. You can place multiple GitLabSOS files from different nodes.
1. Run `docker compose up -d`.
1. Run `./init.sh`.
1. Open `http://localhost:8088/superset/dashboard`. The username and password is `admin`.

## Cleanup

1. Run `docker compose stop && docker compose rm -f`

## Screenshots

![example-01 dashboard](/images/example-01.png)

## How to create a dashboard

### Creating a chart

1. Click the **Charts** tab.
1. Choose a dataset to be used.
1. Choose a visualization type.

  * In this example, we are going to create a Line Chart from the `druid.production_json` dataset.

![create-chart-01](/images/create-chart-01.png)

1. Set the **Time Grain**. This will define how the logs are aggregated in terms of time.
1. Set the **Time Range**. This will define which data is used depending on the time on the logs.

  * In this example, we set the **Time Grain** to **1 minute** and **Time Range** to **No filter**.

![create-chart-02](/images/create-chart-02.png)

1. Fill up the **Query** section depending on the data you want to plot.

  * In this example, we want to plot the number of requests a user created.
  * We also filtered out usernames that are set to `null`.

![create-chart-03](/images/create-chart-03.png)

1. Click **Run** to view the chart.
1. Click **Save** to save the chart.

  * The chart is now ready to be used in a dashboard.

### Creating a dashboard

Creating a dashboard should be straight forward. Just drag and drop the chart you created above.

### Submit a dashboard in this repository

1. Click the **Dashboards** tab.
1. Navigate to the **Actions** field of the desired dashboard.
1. Click the **Export** button.
![export-dashboard-01](/images/export-dashboard-01.png)
1. Place or overwrite the JSON file in the `./dashboards` directory.


## Troubleshooting

### Stucked in loading data into Apache Druid

If you noticed that the script is stucked in this state:

```
Completed indexing data for sidekiq. Now loading indexed data onto the cluster...
sidekiq is 0.0% finished loading...
sidekiq is 0.0% finished loading...
sidekiq is 0.0% finished loading...
sidekiq is 0.0% finished loading...
sidekiq is 0.0% finished loading...
sidekiq is 0.0% finished loading...
sidekiq is 0.0% finished loading...
```

This probably means that the `sidekiq/current` log in the GitLabSOS file is not a valid JSON.

## Notes

Current implementation only provides a dashboard for `production_json.log` and `api_json.log`

## Todo

Add dashboard for the following logs:

* `sidekiq/current`
* `gitaly/current`
